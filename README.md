# GC-HERO

*Disclaimer : this project is a for-fun project made for educational purposes. For a mature tool use https://github.com/ToadKing/wii-u-gc-adapter*

Welcome the the **GC-HERO PROJECT** repository ! 

This project aims at making the official Nintendo GC adapter works on Linux, by creating a virtual device. 


## Compilation

Simply run *make* : the program will be generated in the *bin* folder.

You need the following dependencies (with their developement headers) :

* libusb
* libevdev

To install them on fedora, you can run :

```sh
sudo dnf install libusb-devel libevdev-devel
```

## Running

Just run *bin/gc-hero*. Try running with *sudo* if you have permissions problems.

The program will create 4 virtual devices, each representing a gamecube controller.


## Supported features

* Actions buttons (A, B, X, Y)
* C-stick
* D-pad
* Left stick
* Triggers (L, R, ZR) pression and depth
* Start button

Rumble is not yet supported (coming soon ?)


## System install 

**Warning : untested**

Run `make install` with root privileges. This should do the following :

* Copy an udev rule and a systemd service on the correct locations of your system
* Copy the *bin/gc-hero* executable on your system
* Refresh udev rules and systemd services

In theory, *gc-hero* should launch when plugging the adapter.

Run `make uninstall` with root privileges to undo the above.
