CFLAGS = -Wall -Werror $(shell pkg-config --cflags libusb-1.0) $(shell pkg-config --cflags libevdev)
LDFLAGS = $(shell pkg-config --libs libusb-1.0) $(shell pkg-config --libs libevdev) -lpthread
CC = gcc

TARGET = bin/gc-hero
OBJS = $(patsubst src/%.c, src/%.o, $(wildcard src/*.c))


%.o: %.c
	$(CC) -g -c -o $@ $< $(CFLAGS)

$(TARGET): $(OBJS)
	mkdir -p bin
	$(CC) -o $@ $^ $(LDFLAGS)

all: $(TARGET)

install:
	mkdir -p /opt/gc-hero
	cp bin/gc-hero opt/gc-hero/gc-hero
	cp utils/99-gcadapter.rules /etc/udev/rules.d/99-gcadapter.rules
	cp utils/gc-hero.service /etc/systemd/user/gc-hero.service
	udevadm control --reload-rules
	systemctl daemon-reload

uninstall:
	rm -rf /opt/gc-hero
	rm /etc/udev/rules.d/99-gcadapter.rules
	rm /etc/systemd/user/gc-hero.service
	udevadm control --reload-rules
	systemctl daemon-reload

clean: 
	rm -rf $(TARGET)
	rm -rf $(OBJS)

.PHONY: all clean
